terraform {

  required_version = ">= 0.12.7"

  required_providers {

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }

    google = {
      source  = "hashicorp/google"
      version = "~> 3.28.0"
    }

  }
# backend "gcs" {
#   bucket  = "tf-states-gitlab-devops-projects"
#   prefix  = "rundeck"
# }
  
}

#----------------------------------------------------------------
#  PREPARE PROVIDERS
#----------------------------------------------------------------
provider "google" {
  project = var.project

  scopes = [
    # Default scopes
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/cloud-platform",
    "https://www.googleapis.com/auth/ndev.clouddns.readwrite",
    "https://www.googleapis.com/auth/devstorage.full_control",

    # Required for google_client_openid_userinfo
    "https://www.googleapis.com/auth/userinfo.email",
  ]
}

#----------------------------------------------------------------
#  Configure kubectl
#----------------------------------------------------------------
# configure kubectl with the credentials of the GKE cluster

provider "kubernetes" {
  config_path = "~/.kube/config"
}

#----------------------------------------------------------------
#  Create a deployment object to deploy hpa-test
#----------------------------------------------------------------

resource "kubernetes_deployment" "hpa-test" {
  metadata {
    name = "hpa-test-infra-app"
    namespace  = var.namespace
    labels = {
      app = "hpa-test-app"
    }
  }

  spec {
    replicas = "1"

    selector {
      match_labels = {
        app = "hpa-test-app"
      }
    }

    template {
      metadata {
        labels = {
          app = "hpa-test-app"
        }
      }

      spec {
        image_pull_secrets {
           name =  "docker-registry-tt4y"
        }
 
        container {
          image = "docker build -t registry.gitlab.com/myjourneyiac/k8s-hpa-test/master:latest"
          name  = "hpa-test"
          resources {
             limits = {
               cpu    = "0.5"
               memory = "128Mi"
             }

             requests = {
                cpu    = "125m"
                memory = "50Mi"
             }
          }

          port {
             container_port = 4000
          }

          env_from {
             config_map_ref {
                name = "hpa-test"
             }
          }

          liveness_probe {
             http_get {
                path = "/"
                port = 4000
                scheme = "HTTP"
             }
             initial_delay_seconds = 500
             period_seconds        = 120
          }

          readiness_probe {
            http_get {
               path = "/"
               port = 4000
               scheme = "HTTP"
            }

            initial_delay_seconds = 20
            period_seconds        = 5
          }
        }
      }
    }
  }
}


#----------------------------------------------------------------
#  Create a deployment object to deploy hpa-test
#----------------------------------------------------------------

resource "kubernetes_config_map" "hpa-test" {
  metadata {
    name = "hpa-test"
    namespace  = var.namespace
  }

  data = {
    URL = "https://google.cl/"
  }

}



#----------------------------------------------------------------
#  Create service object for hpa-test
#----------------------------------------------------------------

resource "kubernetes_service" "hpa-test-svc" {
  metadata {
    name        = "hpa-test-svc"
    namespace   = var.namespace
    annotations = {
       "cloud.google.com/neg" = "{\"ingress\": true}"
       "cloud.google.com/load-balancer-type" = "external"
       "cloud.google.com/internal-load-balancer-subnet" = "qa-lb-subnet"
    }
  }
  spec {
    selector = {
      app = kubernetes_deployment.hpa-test.spec[0].template[0].metadata[0].labels.app
    }

    port {
      port        = 4000
      target_port = 4000
    }

    type = "ClusterIP"
  }

  depends_on = [kubernetes_deployment.hpa-test]
}


#----------------------------------------------------------------
#  Create a ingress object for hpa-test
#----------------------------------------------------------------

resource "kubernetes_ingress" "hpa-test-ingress" {
  metadata {
    name        = "hpa-test-ing"
    namespace   = var.namespace
    annotations = {
      "kubernetes.io/ingress.class" = "external"
      "konghq.com/strip-path"       = "true"
      "konghq.com/preserve-host"    = "true"
    }
  }

  spec {
    rule {
        host = "hpa.colmena.cl"
        http {
          path {
            path = "/"
            backend {
              service_name = "hpa-test-svc"
              service_port = 4000
            }
          }
        }
      }
  }

  depends_on = [kubernetes_service.hpa-test-svc ]
}


#----------------------------------------------------------------
#  Create a HPA object for test
#----------------------------------------------------------------
resource "kubernetes_horizontal_pod_autoscaler" "hpa-test" {

  metadata {
    name        = "hpa-test"
    namespace   = var.namespace
  }

  spec {
    min_replicas = 2
    max_replicas = 100

    scale_target_ref {
      api_version = "autoscaling/v2beta2"
      kind = "Deployment"
      name = "hpa-test"
    }
  
    metric {
      type = "External"
      external {
        metric {
          name = "latency"
          selector {
            match_labels = {
              lb_name = "hpa-test"
            }
          }
        }
        target {
          type  = "Value"
          value = "95"
        }
      }
    }
  }
  depends_on = [kubernetes_deployment.hpa-test]
}
