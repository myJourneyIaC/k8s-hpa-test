FROM golang:1.13 as builder

WORKDIR /app

COPY src/*.go ./

RUN go get golang.org/x/time/rate
RUN CGO_ENABLED=0 GOOS=linux go build -v -o simple-hpa-test-app

EXPOSE 8080

RUN rm *.go && chmod +x simple-hpa-test-app

# Run the executable
CMD ["./simple-hpa-test-app"]
